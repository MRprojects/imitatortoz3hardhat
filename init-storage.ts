const { ethers } = require("hardhat");

/********************************************************************
*********************************************************************
    Usual main 
*********************************************************************
********************************************************************/


async function main() {

    const [wallet, other1, other2, other3, other4, other5, other6] = await ethers.getSigners();

    // console.log([wallet.address, other1.address, other2.address]);

    console.log('===> Getting the Factory contract...\n');
    const storage = await ethers.getContractAt('Storage','0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0');

    console.log('===> Initialisation of the storage contract...\n');


    console.log("setting new values...");
    await storage.set(359);
    await storage.connect(other1).set(6241);
    await storage.connect(other3).set(534);
    await storage.connect(other4).set(17);
    await storage.connect(other5).set(123);
    await storage.connect(other6).set(32);
    // console.log("last input is  ", await storage.get());

    console.log("getting all stored values...");
    var tableVal = await storage.getAll();

    console.log("print all stored values...");
    for (let i = 0; i < tableVal.length; i++) {
        console.log("value ",i," is ", tableVal[i].toString());
    }

    // SOME TESTS...
    // console.log("get value 234, (assumed above tableVal.length = ",tableVal.length," )...");
    // console.log("value 234 is ", await storage.getAtIndex(234));
    // console.log("get value -1 ...");
    // console.log("value -1 is ", await storage.getAtIndex(-1));

    console.log("done.");

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });
