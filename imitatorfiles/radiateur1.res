(*
  Result output by IMITATOR
  Version  : IMITATOR 2.7.3 "Butter Guéméné" (build 1338)
  Model    : '/home/pira/radiateur1.imi'
  Generated: Wed Mar 13, 2024 23:01:48
  Command  : imitator -mode EF /home/pira/radiateur1.imi -output-result
*)

 b >= 3
& max_t_r1 >= 0
& total_cost >= 0
& min_t_r1 >= 0
