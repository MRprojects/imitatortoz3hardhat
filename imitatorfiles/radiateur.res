(*
  Result output by IMITATOR
  Version  : IMITATOR 2.7.3 "Butter Guéméné" (build 1338)
  Model    : '/home/pira/radiateur.imi'
  Generated: Mon Mar 18, 2024 13:40:31
  Command  : imitator -mode EF /home/pira/radiateur.imi -output-result
*)

 min_t_r2 >= 0
& max_t_r2 >= min_t_r2
& min_t_r1 >= 0
& max_t_r1 + min_t_r2 + 2 >= min_t_r1 + max_t_r2
& max_t_r1 >= min_t_r1
& total_cost = 3
